This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Smartgears Distribution Bundle

### [v3.5.0]

- Creating distribution bundle for smartgears-distribution 3.5.0-SNAPSHOT

## [v3.0.0] [r4.26.0] - 2020-10-29

- Switched JSON management to gcube-jackson [#19283]
- Fixed distro files and pom according to new release procedure


## [v2.5.4] [r4.23.0] - 2020-06-19

- Released to have and up-to-date bundle 


## [v2-5.3] [r4.22.0] - 2020-05-07

- Released to have and up-to-date bundle


## [v2.5.1] [r4.21.0] - 2020-03-30

- Released to have and up-to-date bundle


## [v2.5.0] [r4.19.0] - 2020-02-04

- Aligned version with smartgears-distribution

## [v2.2.0] [r4.15.0] - 2019-11-08

- Fixed distro files and pom according to new release procedure


## [v2.1.0] [r4.3.0] - 2017-03-16

-  Fixed README file


## [v2.0.0] [r4.1.0] - 2016-12-15

- Upgraded smartgeras-distribution to 2.X.X 


## [v1.0.6] [r3.10.1] - 2016-04-08

- Environment variable values replaced ~/.bashrc instead of appended [#2179]


## [v1.0.5] [r3.10.0] - 2016-02-08

- Improved setup [#77]


## [v1.0.4] [r3.9.0] - 2015-12-09

- Providing smartgears-distribution version as argument when compiling
- Used tomcat 7 range instead of fixed version


## [v1.0.3] [r3.8.0] - 2015-09-11

- Reorganized package creation


## [v1.0.2] - 2015-05-15

- Reorganized package creation


## [v1.0.1] - 2015-04-15

- Adding Bundle Version on container.xml to be published on ghn profile


## [v1.0.0] - 2015-03-11

- First Release

